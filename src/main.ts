import { createApp } from 'vue'
import App from './App.vue'
import example from './plugins/example'

createApp(App).use(example,{
    message:'Hello world'
}).mount('#app')
